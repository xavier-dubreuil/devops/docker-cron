#!/usr/bin/env python3

from docker import APIClient, DockerClient
from socket import gethostname
from sys import argv, exit, stdout
from os import environ


def get_rancher_filter(container, service):
    return {
        'label': [
            'io.rancher.project.name=' + container.labels['io.rancher.project.name'],
            'io.rancher.project_service.name=' + container.labels['io.rancher.project.name'] + '/' + service,
            'io.rancher.stack.name=' + container.labels['io.rancher.stack.name'],
            'io.rancher.stack_service.name=' + container.labels['io.rancher.stack.name'] + '/' + service,
        ]
    }


def get_compose_filter(container, service):
    return {
        'label': [
            'com.docker.compose.project=' + container.labels['com.docker.compose.project'],
            'com.docker.compose.project.config_files=' + container.labels['com.docker.compose.project.config_files'],
            'com.docker.compose.project.working_dir=' + container.labels['com.docker.compose.project.working_dir'],
            'com.docker.compose.service=' + service,
        ]
    }


def docker_exec(service, user, command):
    docker = DockerClient()
    hostname = gethostname()
    orchestrator = environ['ORCHESTRATOR'] if 'ORCHESTRATOR' in environ else 'compose'

    try:
        container = docker.containers.get(hostname)
    except:
        return False, 1, 'Current container not found'

    try:
        if orchestrator == 'compose':
            filters = get_compose_filter(container, service)
        elif orchestrator == 'rancher':
            filters = get_rancher_filter(container, service)
        else:
            return False, 2, 'Unhandle orchestrator: ' + orchestrator
    except:
        return False, 4, 'Missings labels on container'

    containers = docker.containers.list(filters=filters)
    if len(containers) == 0:
        return False, 3, 'Target container not found'

    container = containers[0]

    exit_code, output = container.exec_run(command, user=user)
    return True, exit_code, output


if __name__ == '__main__':
    argv.pop(0)
    service = argv.pop(0) if len(argv) > 0 else None
    user = argv.pop(0) if len(argv) > 0 else None
    user = user if len(user) > 0 else 'root'
    command = argv
    executed, exit_code, output = docker_exec(service, user, command)
    log = 'Cron [' + service + '][' + user + ']: ' + ' '.join(command) + '\n'
    if executed:
        log += '  - Exit status: ' + str(exit_code) + '\n'
        log += '  - Output: >>>' + '\n'
        log += output.decode('utf-8')
        log += '<<<<' + '\n'
    else:
        log += ' - ERROR: Not Executed: ' + output
    print(log)
