FROM debian:10.7-slim

RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections && \
    apt-get update && \
    apt-get upgrade -y && \
    apt-get install --no-install-recommends -y cron python3 python3-pip && \
    pip3 install docker && \
    apt-get purge -y --auto-remove && \
    rm -rf /var/lib/apt/lists/*

COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint
COPY cronjob.py /usr/local/bin/cronjob

RUN chmod +x /usr/local/bin/docker-entrypoint /usr/local/bin/cronjob

ENTRYPOINT ["docker-entrypoint"]
