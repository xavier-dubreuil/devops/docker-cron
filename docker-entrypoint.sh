#!/usr/bin/env bash

mv /etc/crontab /etc/crontab.dist

{
  echo "SHELL=/bin/bash"
  echo "PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin"
  echo "ORCHESTRATOR=${ORCHESTRATOR}"
  echo ""
} > /etc/crontab

for cronjob in "$@"
do
    service=$(echo "${cronjob}" | cut -d':' -f1)
    user=$(echo "${cronjob}" | cut -d':' -f2)
    schedule=$(echo "${cronjob}" | cut -d':' -f3)
    command=$(echo "${cronjob}" | cut -d':' -f4-)

    echo "${schedule} cronjob ${service} ${user:-root} ${command} 1>/proc/1/fd/1 2>/proc/1/fd/2" >> /etc/crontab
done

echo "Content of Crontab:"
cat /etc/crontab

# Registering the new crontab
echo "Registering crontab"
crontab /etc/crontab

# Starting the cron
echo "Starting crontab"
cron -f

}
